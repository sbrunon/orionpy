import json
import unittest

from orionpy.orioncore.Exceptions import BuildFilterError
from orionpy.orioncore.Filters import Filters


class TestFilters(unittest.TestCase):
    def setUp(self):
        self.filters = Filters()

    def test_update(self):  # TODO test
        pass

    def test_label_already_used(self):
        filtering_names = [{'name': 'label'}, {'name': 'titi'}, {'name': 'toto'}]
        self.assertTrue(self.filters._label_already_used(filtering_names, 'label'))
        self.assertFalse(self.filters._label_already_used(filtering_names, 'NOT HERE'))

    def test_build_partition_1_attr(self):
        model = [{'name': 'val1', 'properties': {'attr': 'val1'}},
                 {'name': 'val2', 'properties': {'attr': 'val2'}}]
        res = self.filters._build_partition_1_attr('attr', ['val1', 'val2', 'val2'])
        for expected, received in zip(model, res):
            self.assertDictEqual(expected, received)

    def test_build_partitions_attributes(self):
        model = [{'name': 'v11', 'properties': {'a1': 'v11', 'a2': 'v12'}},
                 {'name': 'v21', 'properties': {'a1': 'v21', 'a2': 'v22'}},
                 {'name': 'v31', 'properties': {'a1': 'v31', 'a2': 'v32'}}]

        res = self.filters._build_partitions_attributes(['a1', 'a2'], [['v11', 'v12'],
                                                                       ['v21', 'v22'],
                                                                       ['v21', 'v22'],
                                                                       ['v31', 'v32']])
        for expected, received in zip(model, res):
            self.assertDictEqual(expected, received)

        self.assertIsNone(self.filters._build_partitions_attributes(['a'], [['a', 'b']]))

    def test__build_list_filtering_values(self):
        self.assertIsNone(self.filters._build_list_filtering_values([], []))
        self.assertIsNotNone(
            self.filters._build_list_filtering_values(['a1', 'a2'], [['v11', 'v12'],
                                                                     ['v21', 'v22'],
                                                                     ['v31', 'v32']]))
        self.assertIsNotNone(self.filters._build_list_filtering_values('attr', ['val1', 'val2']))

    def test_add_FDU_filter(self):
        self.filters.add_FDU_filter('fname', ['attr', 'atr2'], ['val2'])
        self.assertRaisesRegex(BuildFilterError, "[ERROR FDU] Error while creating partition")

    def test_add_SQL_filter(self):
        self.filters.add_SQL_filter('fname', '')
        self.assertRaises(BuildFilterError)

    def test_create_filter(self):
        self.filters.create_filter()

    def test_add_list(self):  # TODO tests
        pass

    def test_prepare_request(self):
        class AD:
            def __init__(self):
                self.el = 'a'
                self.el2 = 1
        a = AD()
        expected = {'el': 'a', 'el2': 1}
        self.filters._filters_as_dic = []
        self.filters._prepare_request(a)
        self.assertDictEqual(expected, self.filters._filters_as_dic[-1])

    def test_filters_formatted(self):
        self.filters._filters_as_dic = [{'el': 'a', 'el2': 1}]
        expected = {'dimensions': [{'el': 'a', 'el2': 1}]}
        self.assertDictEqual(expected, json.loads(self.filters._filters_formatted()))

    def test_filter_exist_already(self):
        self.assertTrue(self.filters._filter_exist_already('here', ['a', 'here', 'g']))
        self.assertFalse(self.filters._filter_exist_already('wrong', ['a', 'f']))
