# coding: utf-8

import sys
from sys import exit

# TODO make the following imports cleaner.
# TODO rename "src" --> orionpy ?
from orionpy.orioncore.Orion import Orion
from orionpy.orioncore.resources.Resource import RightLevel
from orionpy.orioncsv.csvfilteringvalues import CSVFilteringValues
from orionpy.oriongis.oriongis import OrionGIS
from orionpy.orioncsv.csvrightsmanagement import CSVRightsManagement
from orionpy.orioncsv.csvfilteringcadastre import CSVFilteringCadastre


class Usage(Exception):
    def __init__(self, msg):
        self.msg = msg


def test_filters(orion):
    # ------- Filter tests -------
    print('\n\nTesting filters\n')
    # TODO figure out a better access to a field ! (orion.resources.get(path_to_field) ?)
    # field = orion.services.get('Cannes/EspacesVerts/MapServer').layers.get('Arbres').fields.get('etat')
    orion.filters.add_SQL_filter('SQL_filter', where_clause = 'POP_RANK=2')
    orion.filters.add_FDU_filter('FDU_typeH', fields = ['type'], filtering_values = ['Herbes'])
    orion.filters.add_FDU_filter('FDU_FILS', fields = ['type', 'etat'],
                                 filtering_values = [['Autres', 'EC'], ['Herbes', 'NET']])
    print('')
    # print again the list to check if changes applied
    for filt in orion.filters.all():
        print(filt)
    # Get a particular filter
    filt = orion.filters.get('FDU_typeH')
    print(filt.get_fields())
    print(filt.get_labels())

    print('\n', filt)
    return filt


def test_filter_value(orion):
    # TODO : change filtername, groupname and values ifneedbe
    cadastre_filter = orion.filters.get("filtre par commune")
    group = orion.groups.get("Aurora1_group")

    print(group.get_activated_labels(cadastre_filter))
    group.set_filter_field_values(cadastre_filter, ["060046", '060060'], "id_comm")
    print(group.get_activated_labels(cadastre_filter))
    group.set_filter_values(cadastre_filter, ["NICE"])
    print(group.get_activated_labels(cadastre_filter))


def test_services(orion):
    # ------- Service Tests -------
    # get list of service names
    print('\n\nTesting services\n')
    managed_service = None
    services_urls = orion.services.urls()
    # get list of services
    print(services_urls)
    services = orion.services.all()
    for s in services:
        print(s)
    print('\nAnd now, only managed services : ')
    managed_services = orion.services.all_managed()
    for s in managed_services:
        print(s)
    # todo FIX if not fed
    if managed_services:  # if list not empty
        service_url = list(managed_services)[0].get_access_url()
        managed_service = orion.services.get_in_managed(service_url)
        if managed_service is None:
            print('ERROR getting 1 managed service')
            exit(-2)
        if managed_service.get_access_url() != service_url:
            print('ERROR in test services "', managed_service.get_access_url(), '" != "',
                  service_url)
            exit(-2)
        print('\n', service_url, " : ", managed_service)

    # get 1 particular service
    service = orion.services.get(services_urls[0])
    if service is None:
        print('ERROR getting 1 service')
        exit(-2)
    if services_urls[0] != service.get_access_url():
        print('ERROR in test services "', services_urls[0], '" != "', service.restRelativeUrl, '"')
        exit(-2)
    print(services_urls[0], " : ", service)
    if managed_services:
        return managed_service
    else:
        return service


def test_groups(orion):
    # ------- Group Tests -------
    print('\n\nTesting groups\n')
    for g in orion.groups.all():
        print(g)
    # Two ways to get a group
    # ID : 6e55675e1aa14318ad2051a971a8d9d4, ecf5ee3f31724dddade485822c707166 or b53f5db9c53c4c36a2e88f2adcd34bd3
    same_group = [orion.groups.get_with_id('a8b06d2adfb642f7965eb91a8f809a78'),
                  orion.groups.get('Featured Maps and Apps')]
    print()
    for g in same_group:
        if g is None:
            print('ERROR in test groups')
            exit(-2)
        print(g)

    return same_group

def test_users(orion):
    # ------- User Tests -------
    print('\n\nTesting users\n')
    users = orion.users.all()
    for u in users:
        print(u)
    print(len(users))
    # Two ways to get a user
    # ID : 6e55675e1aa14318ad2051a971a8d9d4, ecf5ee3f31724dddade485822c707166 or b53f5db9c53c4c36a2e88f2adcd34bd3
    same_user = [orion.users.get_with_id('47e44be4887b4ed4addcd6339d298134'),
                  orion.users.get('editor_aob')]
    print()
    for u in same_user:
        if u is None:
            print('ERROR in test users')
            exit(-2)
        print(u)

    return same_user

def test_layers(service):
    # ------- Layers Tests -------
    print('\n\nTesting layers\n')
    # Get all layers
    layers = list(service.layers.all())
    for layer in layers:
        print(layer)

    # Get 1 particular layer and get its id
    layer_name = layers[0].name

    layer = service.layers.get(layer_name)
    if layer is None:
        print('Error getting 1 layer')
        exit(-2)
    print("")
    print(layer, end = ". Check ID is ")
    layer_id = service.layers.get_id(layer_name)
    print(layer_id)
    print('With ID : ', service.layers.get_with_id(layer_id))

    return layer


def test_group_layers(orion):
    # ------- Group of layers Tests -------
    service = orion.services.get("Tests/SBR_Tests_MapServer")
    layer_g = service.layers.get_with_id(0)
    layer_ng = service.layers.get_with_id(' 2 ')

    # Following should print an error message and no fields
    for field in layer_g.fields.all():
        print(field)

    print(layer_g)
    print(layer_ng)
    print(layer_g.get_parent_layer_id() is None)
    print(layer_ng.get_parent_layer_id() == 0)
    print(not layer_g.has_parent_layer())
    print(layer_ng.has_parent_layer())
    print(layer_g.has_sub_layers())
    print(not layer_ng.has_sub_layers())
    print(layer_g.get_sub_layers_ids())
    print(layer_ng.get_sub_layers_ids() is None)


def test_tables(service):
    for table in service.tables.all():
        print(table)


def test_fields(layer):
    # ------- Fields Tests -------
    print('\n\nTesting fields\n')
    # Get all layers
    fields = list(layer.fields.all())
    for field in fields:
        print(field)

    # Get 1 particular field
    field = layer.fields.get(fields[0].name)
    if field is None:
        print('Error getting 1 field')
        exit(-2)
    print('\n', field)

    # Change inheritance
    return field


def test_print_all_resources(orion):
    """Test function printing all resources
    """
    for service in orion.services.all():
        print(service)
        for layer in service.layers.all():
            print('\t', layer)
            for field in layer.fields.all():
                print('\t\t', field)
        for table in service.tables.all():
            print('\t', table)
            for field in table.fields.all():
                print('\t\t', field)


def test_update_right(resource, group):
    # TODO make it work with a non-federated system
    # TODO make this test work with other resource type
    # TODO make sure that resource shared with given group
    """Test every possible change of rightlevel"""
    print('SETUP')
    resource.enable()  # TODO whatif not a service ?
    resource.disable_inheritance(group)
    resource.update_right(group, RightLevel.ACCESS)
    print('ACCESS -> ACCESS')
    resource.update_right(group, RightLevel.ACCESS)
    resource.print_rights(group)
    print('ACCESS -> READ')
    resource.update_right(group, RightLevel.READ)
    resource.print_rights(group)
    print('READ -> READ')
    resource.update_right(group, RightLevel.READ)
    resource.print_rights(group)
    print('READ -> ACCESS')
    resource.update_right(group, RightLevel.ACCESS)
    resource.print_rights(group)
    print('ACCESS -> WRITE')
    resource.update_right(group, RightLevel.WRITE)
    resource.print_rights(group)
    print('WRITE -> WRITE')
    resource.update_right(group, RightLevel.WRITE)
    resource.print_rights(group)
    print('WRITE -> READ')
    resource.update_right(group, RightLevel.READ)
    resource.print_rights(group)
    print('READ -> WRITE')
    resource.update_right(group, RightLevel.WRITE)
    resource.print_rights(group)
    print('WRITE -> ACCESS')
    resource.update_right(group, RightLevel.ACCESS)
    resource.print_rights(group)

    print('----ERROR CASES----')
    resource.enable_inheritance(group)
    print('\nINHERITANCE ENABLED')
    resource.update_right(group, RightLevel.ACCESS)
    resource.disable_inheritance(group)
    print('\nSERVICE NOT MANAGED')
    resource.disable()
    resource.update_right(group, RightLevel.ACCESS)


def test_force_rights(resource, group):
    """Test every possible change of rightlevel"""
    print('READ -> ACCESS')
    resource.enable_inheritance(group)
    resource.force_right(group, RightLevel.ACCESS)
    resource.print_rights(group)
    print('READ -> READ')
    resource.enable_inheritance(group)
    resource.force_right(group, RightLevel.READ)
    resource.print_rights(group)
    print('READ -> WRITE')
    resource.enable_inheritance(group)
    resource.force_right(group, RightLevel.WRITE)
    resource.print_rights(group)


def test_csv(orion):
    gen = True
    csv_rights = CSVRightsManagement(orion)
    csv_path = '../CSVs/csv_rights_summary.csv'
    if gen:
        service = orion.services.get('arcOpole/arcOpoleBuilder_MapServer')
        group_list = []
        csv_rights.generate(csv_path, service, group_list)
    else:
        csv_rights.read_and_apply(csv_path)

    # Tests for filtering values
    csv_filter = CSVFilteringValues(orion)
    csv_path = '../CSVs/csv_filtering.csv'
    if gen:
        csv_filter.generate(csv_path)
    else:
        csv_filter.read_and_apply(csv_path)

    csv_path = "orionpy/test/result_simple.csv"
    json_path = "orionpy/test/acldatabase.json"
    csv_filtering_cadastre = CSVFilteringCadastre(orion)
    if gen:
        csv_filtering_cadastre.generate(csv_path, json_path)
    else:
        csv_filtering_cadastre.read_and_apply(csv_path)


def test_dis_inhe(orion):
    group = orion.groups.get('Featured Maps and Apps')
    service = orion.services.get('Cannes/EspacesVerts_FeatureServer')
    layer = service.layers.get_with_id('0')

    print('Preparation')
    service.enable()
    service.enable_inheritance(group)
    service.print_rights(group)

    print('simple disable (READ)')
    service.disable_inheritance(group)
    service.print_rights(group)

    print('Preparation')
    service.force_right(group, RightLevel.WRITE)
    layer.enable_inheritance(group)
    layer.print_rights(group)
    print('parent = WRITE')
    layer.disable_inheritance(group)
    layer.print_rights(group)

    print('Preparation')
    service.force_right(group, RightLevel.ACCESS)
    layer.enable_inheritance(group)
    layer.print_rights(group)
    print('parent = ACCESS')
    layer.disable_inheritance(group)
    layer.print_rights(group)


def test_cadastre(orion):
    cadastre_resource = orion.businesses.get_cadastre_resource()
    if cadastre_resource is None:
        print("A Cadastre resource isn't configured !")
        exit(-2)
    print(cadastre_resource)

    # Get associated filter
    print(cadastre_resource.associated_filter_id)
    # TODO method orion.filters.get_cadastre_filter() instead ?
    associated_filter = orion.filters.get_with_id(cadastre_resource.associated_filter_id)
    print(associated_filter)

    cadastre_resource.init_filter_access(associated_filter)

    # Test for updating rights
    print(cadastre_resource.associated_filter)
    access_group = orion.groups.get("Organisation")
    # public_group = orion.groups.get("Nice non-nominatif")
    # nominative_group = orion.groups.get("Colomars nominatif")

    cadastre_resource.print_rights(access_group)

    cadastre_resource.update_right(access_group, RightLevel.ACCESS)
    cadastre_resource.print_rights(access_group)

    cadastre_resource.update_right(access_group, RightLevel.NOMINATIF_ACCESS)
    cadastre_resource.print_rights(access_group)

    # cadastre_resource.print_rights(public_group)
    # cadastre_resource.print_rights(nominative_group)
    # Test filter activation / deactivation
    cadastre_resource.activate_filter(access_group)
    cadastre_resource.deactivate_filter(access_group)



def main():
    # ------- Orion connection tests -------
    username = "admin_aob"  # Enter here username
    password = "1arcOpole!"  # If you want, enter password (if empty, will be asked - securely - later)
    url_machine = "https://fdev.ds-esrifrance.fr"  # External url of the WebAdaptor used
    portal = "portal"  # Entry point to the WebAdaptor of the Portal for ArcGIS (optional)

    orion = Orion(username, password, url_machine, portal, verify_cert = False)
    csv_fv = CSVFilteringValues(orion)
    csv_fv.read_and_apply("./filtering_values.csv")
    csv_fv.generate('./filtering_values.csv')
    sys.exit(0)

    service = orion.services.get("Cannes/EspacesVerts_FeatureServer")
    print(orion.services_gis_mgr.get_groups_id_shared(service = service))

    filt = test_filters(orion)
    groups = test_groups(orion)
    service = test_services(orion)
    test_tables(service)
    layer = test_layers(service)
    field = test_fields(layer)
    test_update_right(service, groups[0])
    test_force_rights(service, groups[0])
    test_csv(orion)
    test_dis_inhe(orion)

    return 0


if __name__ == '__main__':
    sys.exit(main())
