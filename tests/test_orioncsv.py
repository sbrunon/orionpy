from orionpy.orioncsv.orioncsv import OrionCSV

import unittest
import csv


class TestOrionCSV(unittest.TestCase):
    def setUp(self):
        self.orioncsv = OrionCSV(None)
        self.csv_path = "./csv_test"

    def test_get_csv_writer(self):
        with open(self.csv_path, "w+", newline = '') as csv_file:
            self.assertIsInstance(self.orioncsv.get_csv_writer(csv_file, []), csv.DictWriter)

    def test_get_csv_reader(self):  # TODO tests
        # with open(self.csv_path, "r+") as csv_file:
            # self.assertIsNotNone(self.orioncsv.get_csv_reader(None))
        pass
