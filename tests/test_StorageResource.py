import unittest
import responses

from orionpy.orioncore.Group import Group
from orionpy.orioncore.resources.StorageResource import StorageResource
from orionpy.orioncore.resources.Resource import RightLevel


class TestStorageResource(unittest.TestCase):
    def setUp(self):
        self.resource = StorageResource({
            "dimensionId":"",
            "id":"standard",
            "service": {
                "url":"Hosted/StatService/FeatureServer"
            },
            "tables": [   ],
            "type":"service"
        })
        
        responses.add(responses.POST, "https://front.arcopole.fr/Orion/orion/admin/tree/object/BUSINESS/Stats/standard/__configure", status = 200)

    def test_instance(self):  
        self.assertTrue('standard' in str(self.resource))

    @responses.activate
    def test_update_filter(self):
        self.assertEqual("", self.resource.filter_id)

        self.resource.update_filter("new_filter")

        self.assertEqual("new_filter", self.resource.filter_id)


